# LHEAnalyzer

This is a basic python script to analyze Les Houches-Accord Event Files. It has been tested on leading-order MadGraph5_aMC event files. 

The syntax is:

```
python3 lhe_analyzer.py [lhe file] ([output file tag])
```
where ```[lhe file]``` is the Les Houches-accord event file to analyze and ```([output file tag])``` is the (optional) output tag for any generated histograms.

The analysis code should be written in the ```analyze()``` function. 

An example .lhe file is provided, generated via MG5_aMC in a model with an additional scalar, eta0 (99925), with the process:  mu+ mu- > eta0 mu+ mu-.

See the code for further details on usage and possible limitations. 
